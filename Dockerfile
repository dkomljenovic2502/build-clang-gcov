# vim: ts=4 sw=4 expandtab:
#
# DOCKER_BUILDKIT=1 docker build -t git.zenitel.com:5005/st/docker-images/station-sdks-imx6:2020.08.06 -f Dockerfile-x64_64 .
#


FROM archlinux as builder

LABEL MAINTAINER Darko Komljenovic <dkomljenovic@zoho.com>

# install required packages
RUN pacman --quiet --noconfirm --needed -Syu \
    base-devel \
    boost \
    check \
    cmake \
    ninja \
    gcovr \
    clang \
    git

# tear out locales to save some space
RUN find /usr/share/locale -type f -exec rm {} \;

# second stage will copy the updated files from the builder to avoid having
# to have multiple layers and artifacts in the production image. Please note
# that this has to be done in three steps since copy does not actually copy
# folder, just the contents.
FROM archlinux
COPY --from=builder /etc /etc
COPY --from=builder /usr /usr
COPY --from=builder /opt /opt
COPY --from=builder /root /root